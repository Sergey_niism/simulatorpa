#include "widget.h"
#include "ui_widget.h"



//#define resiverPort 5555
//#define senderPort 3333

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);

    resiverDatagramsize = RESIVER_DATA_SIZE RESIVER_DATA_SIZE;
    pointRange = POINT_RANGE;

    sendLine.resize(SENDER_DATA_SIZE,0);
    sent_dzetta = SENT_DZETTA ;
    sent_tetta = SENT_TETTA;
    sent_marsh = SENT_MARSH;

    logData = new LogData(resiverDatagramsize);
    log_time = LOG_TIME;
    log_tetta = LOG_TETTA;
    log_dzetta = LOG_DZETTA;



    f_mission = 0;
    f_simulation = 0;



    udpResiver = new UdpClient(RESIVER_PORT,this);
    udpSender = new UdpServer(SENDER_PORT,this);

    mission = 0;



    ui->label_resiver->setStyleSheet("background-color:rgb(200, 0, 0)");
    ui->label_sender->setStyleSheet("background-color:rgb(150, 150, 150)");

    ui->lineEdit_IP->setText(QHostAddress(QHostAddress::LocalHost).toString());
    ui->lineEdit_reciver->setText(QString::number(RESIVER_PORT));
    ui->lineEdit_sender->setText(QString::number(SENDER_PORT));

    ui->label_prograss->setText("Готов");

    timer = new QTimer(this);

    connect(udpResiver,&UdpClient::S_takeDatagram,this,&Widget::receivedData);
    connect(ui->pushButton_start,&QPushButton::toggled,this,&Widget::pushSimulation);
    connect(timer,&QTimer::timeout,this,&Widget::timeout);
    timer->start(TIMER_TICK*1000);


}

Widget::~Widget()
{
    delete ui;
}

 template<typename type> void Widget::qDebugVector (std::vector <type> in)
{
    QString out;
    out.clear();
    for (int i =0; i<in.size(); i++)
    {
        out+= QString::number(in.at(i))+" ";
    }
    qDebug().noquote()<<out;
}

void Widget::receivedData()
{
    model_disconnect_count = 0;
    f_model_connect = 1;
    ui->label_resiver->setStyleSheet("background-color:rgb(0, 150, 0)");
    QByteArray data = udpResiver->GetData();




    std::vector<resiverType> lineData;
    lineData.clear();
    if (((uint) data.size())!= sizeof(resiverType)*resiverDatagramsize)
    {
        qDebug()<<"Error datagram!!!";
        return;
    }
    resiverType* ptr = (resiverType*) data.data();

    QString out;
    for (int i =0; i< resiverDatagramsize;i++)
    {
        lineData.push_back(ptr[i]);
        out+=QString::number(lineData.back())+"\t";
    }


//    qDebug().noquote()<<out;
    logData->addLine(lineData);
    rtData_X = lineData.at(1);
    rtData_Y = lineData.at(2);

//    out += "\t";
    if (f_mission == 1)
    {
        double range = sqrt(pow((lineData.at(log_tetta)-mission->trajectory.at(pointNumber-1).X),2)
                            +pow(lineData.at(log_dzetta)-mission->trajectory.at(pointNumber-1).Y,2));
//        qDebug()<<"range ="<<range;
        out += QString::number(range);
        if (range < pointRange)
        {
            qDebug()<<"Точка достигнута";
            nextStepSimulation();
        }
    }

    if (ui->pushButton_start->isChecked())
        ui->textBrowser_log->append((out+="\n"));


}

void Widget::pushSimulation()
{
    static int f = 0;

    remove(UDP_DEMON_RTData_NAME);

#if TestMod==1
        f_planner_connect = 1;

#endif

    if (!(f_model_connect&&f_planner_connect))
    {
        if (mission==0)
        {

        if (f == 0)
        {
            f = 1;
            QMessageBox::information(this,"Внимание",
                                     "Ошибка соединения! Нельзя начать моделированиесвязи с моделью и планировщиком");
            ui->pushButton_start->setChecked(false);


        }
        else
        {
            f = 0;
        }
        return;
        }
    }


    if (ui->pushButton_start->isChecked())
    {
#if TestMod==1
        startMission(testData::getTestMission());

#endif

#if TestMod==0
        startMission(mission_from_planner);
#endif


        qDebug()<< "mission start";
        ui->pushButton_start->setText("Прервать миссию");
    }
    else
    {
        if(pointNumber>=mission->trajectory.size())
        {
           QMessageBox::information(this,"Внимание","Моделирование корректно завершено!\n Можно отправить LOG!");
           ui->pushButton_2->setEnabled(true);
        }
        stopMission();
        qDebug()<< "mission stop";
        ui->pushButton_start->setText("Начать миссию");       
    }


}

void Widget::nextStepSimulation()
{
    f_mission = 0;
    pointNumber++;

    qDebug()<< "pointNumber"<<pointNumber;


    if (pointNumber > mission->trajectory.size())
    {
        sendLine.at(sent_marsh) = 3;
        emit StopSimulation();
        qDebug()<<"StopSimulation";
        f_mission = 0;
        sendLine.at(sent_dzetta) = mission->trajectory.at(mission->trajectory.size()-1).X;
        sendLine.at(sent_tetta) = mission->trajectory.at(mission->trajectory.size()-1).Y;
        ui->progressBar_trajectory->setValue(ui->progressBar_trajectory->maximum());
        ui->label_prograss->setText(QString("достигнуто ")+QString::number(pointNumber-1)+QString(" из ")+
                                    QString::number(mission->trajectory.size())
                                    +QString("Включен режим динамического позиционирования"));

        ui->pushButton_start->setText("Закончить");

    }
    else
    {
        sendLine.at(sent_marsh) = 1;
        f_mission = 1;
        sendLine.at(sent_dzetta) = mission->trajectory.at(pointNumber-1).X;
        sendLine.at(sent_tetta) = mission->trajectory.at(pointNumber-1).Y;
        ui->progressBar_trajectory->setValue((pointNumber-1)*100.0/mission->trajectory.size());
        ui->label_prograss->setText(QString("достигнуто ")+QString::number(pointNumber-1)+QString(" из ")+
                                    QString::number(mission->trajectory.size())+QString(""));

    }


    qDebugVector(sendLine);



    QByteArray sendData((char*) sendLine.data(),sendLine.size()*sizeof(senderType));
    udpSender->sendDatagram(sendData);

}

void Widget::startMission(Mission mis)
{
    ui->progressBar_trajectory->setValue(ui->progressBar_trajectory->minimum());
    f_mission = 0;
    pointNumber = 0;



    mission = new Mission(mis);
    ui->label_prograss->setText(QString("достигнуто 0 точек из ")+=QString::number(mission->trajectory.size()));
    ui->pushButton->setEnabled(true);
    ui->pushButton->setChecked(true);

    nextStepSimulation();

}

void Widget::stopMission()
{
    f_mission = 0;
    pointNumber = 0;
    delete mission;
    sendLine.clear();
    sendLine.resize(3,0);
    ui->progressBar_trajectory->setValue(ui->progressBar_trajectory->minimum());
    ui->label_prograss->setText("Готов");

}

void Widget::on_pushButton_log_clicked()
{
    ui->textBrowser_log->clear();
}

int Widget::makeMissionFromData(QByteArray data)
{

    QJsonParseError json_parseError;

    QJsonDocument json_doc;

    json_doc = QJsonDocument::fromJson(data,&json_parseError);

//     qDebug()<<"Error ="<<json_parseError.errorString()<<" = "<< json_parseError.errorString().toInt()<<QJsonParseError::NoError;
    if(json_parseError.errorString().toInt()!=QJsonParseError::NoError)
    {
        QMessageBox::information(this,"Внимание!","Ошибка парсинга!");
        return -1;
    }

    QJsonObject mainObj = json_doc.object();

    QJsonValue trajectoryValue = mainObj.value(json_keyOfTrajectory);

    if (trajectoryValue.isArray()==false)
    {
       QMessageBox::information(this,"Внимание!","Ошибка протокола!");
       return -1;
    }

    QJsonArray array = trajectoryValue.toArray();

    Mission mis;
    mis.trajectory.resize(array.count());

   for(int i=0;i<array.count();i++)
   {
       double x=0, y=0;

       mis.trajectory.at(i).X = array.at(i).toObject().value("x").toDouble();
       mis.trajectory.at(i).Y = array.at(i).toObject().value("y").toDouble();

//       if (array.at(i).toObject().value("view").isUndefined()==true)
//           ui->spinBox->setValue(ui->spinBox->maximum());
//       else ui->spinBox->setValue(array.at(i).toObject().value("view").toInt());


//       if(array.at(i).toObject().value("sem1").isUndefined()==false)
//           P_semantic1 = array.at(i).toObject().value("sem1").toString();
//       else P_semantic1 = "";

//       if(array.at(i).toObject().value("sem2").isUndefined()==false)
//           P_semantic2 = array.at(i).toObject().value("sem2").toString();
//       else P_semantic2 = "";
   }

   mission_from_planner = mis;
   return 0;
}

void Widget::timeout()
{
    checkPlannerData();

    if (ui->pushButton->isChecked())
        sendRTData();

    model_disconnect_count++;

    if (model_disconnect_count>=MODEL_DISCONNECT_COUNT)
    {
        f_model_connect = 0;
        ui->label_resiver->setStyleSheet("background-color:rgb(200, 0, 0)");
    }



}

void Widget::on_pushButton_2_clicked()
{

    QMessageBox::information(this,"Статус","LOG был отправлен на планировщик!");
    ui->pushButton_2->setEnabled(false);
    sendLog();
}

void Widget::sendLog()
{
    char* str = UDP_DEMON_LOG_NAME;
    logData->toFileTXT(str);
    delete str;
}

void Widget::sendRTData()
{
    static int n =0;
    std::ofstream out;
    out.open(UDP_DEMON_RTData_NAME);

    if (!out.is_open())
    {
        n++;
        if (n>5)
        {
            QMessageBox::information(this,"Ошибка","Не получилось отправить кданные реального времени");
        }

        return;
    }

    out<< rtData_X<<"\t"<<rtData_Y;
    out.close();


}

void Widget::checkPlannerData()
{
//    static bool f = 0;

//    if (f)
//        return;

    std::ifstream in;
    in.open(UDP_DEMON_MISSION_NAME,std::ios::in);
    if (in.is_open())
    {
        in.close();
        QMessageBox::information(this,"Пришли данные от планировщика","Пришли данные от планировщика, миссия будет принята");
        QFile qFile;
        qFile.setFileName(UDP_DEMON_MISSION_NAME);

        if (qFile.open(QIODevice::ReadOnly|QFile::Text)==0)
            {
                QMessageBox::information(this,"Внимание!","Ошибка формата миссии!\n Миссия не будет загружена");
            }
        else
        {

            QByteArray json_byteArray;
            json_byteArray = qFile.readAll();
            qFile.close();
            if (makeMissionFromData(json_byteArray)==0)
            {
                //посылка принята успешно

                 f_planner_connect = 1;
                ui->label_resiver_2->setStyleSheet(("background-color:rgb(0, 150, 0)"));

            }
        }


//        f = 1;
        remove(UDP_DEMON_MISSION_NAME);
    }

}
