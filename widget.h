#ifndef WIDGET_H
#define WIDGET_H

#include <fstream>


#include <QWidget>
#include <QDebug>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonDocument>
#include <QJsonValue>
#include <QJsonParseError>
#include <QVariantMap>
#include <QMessageBox>
#include <QFile>
#include <QTimer>
#include <cmath>

#include <SimulationClasses/allinclude.h>
#include <SimulationClasses/testdata.h>


#define TestMod 0


#define TIMER_TICK 0.1
#define MODEL_DISCONNECT_COUNT 1/TIMER_TICK
#define UDP_DEMON_MISSION_NAME "D:\\qwerty\\udpDemon\\mission.json"
#define UDP_DEMON_LOG_NAME "D:\\qwerty\\udpDemon\\log.txt"
#define UDP_DEMON_RTData_NAME "D:\\qwerty\\udpDemon\\rtData.txt"
#define JSON_KEY_OF_MISSION "trajectory"



#define RESIVER_PORT 5555

#define RESIVER_DATA_SIZE 3;
#define RESIVER_DATA_TYPE double

#define SENDER_PORT 3333
#define SENDER_DATA_SIZE 3
#define SENDER_DATA_TYPE double

#define SENT_MARSH 2
#define SENT_TETTA 1
#define SENT_DZETTA 0



#define LOG_TIME 0
#define LOG_TETTA 1
#define LOG_DZETTA 2




#define POINT_RANGE 10

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT
typedef RESIVER_DATA_TYPE resiverType ;
typedef SENDER_DATA_TYPE senderType ;
public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

    template<typename type> static void qDebugVector(std::vector<type> in);

public slots:
    void receivedData();
    void pushSimulation();
    void nextStepSimulation();
    void startMission(Mission mis);
    void stopMission();

private:
    Ui::Widget *ui;

    UdpClient* udpResiver;
    UdpServer* udpSender;

    int resiverDatagramsize;

    LogData* logData;
    int log_time;
    int log_tetta;
    int log_dzetta;


    int sent_dzetta;
    int sent_tetta;
    int sent_marsh;

    int pointRange;

    std::vector<senderType> sendLine;


    bool f_simulation;

    int pointNumber;
    Mission* mission;
    Mission mission_from_planner;
    bool f_mission;

    QTimer* timer;
    QString json_keyOfTrajectory = JSON_KEY_OF_MISSION;

    double rtData_X = 0;
    double rtData_Y = 0;

    bool f_model_connect = 0;
    bool f_planner_connect = 0;
    int  model_disconnect_count = 0;


signals:
    int StopSimulation();


private slots:
    void on_pushButton_log_clicked();
    int makeMissionFromData(QByteArray data);
    void timeout();
    void on_pushButton_2_clicked();
    void sendLog();
    void sendRTData();
    void checkPlannerData();
};

#endif // WIDGET_H
