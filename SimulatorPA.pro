#-------------------------------------------------
#
# Project created by QtCreator 2018-12-17T17:12:32
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SimulatorPA
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        widget.cpp \
    SimulationClasses/mission.cpp \
    SimulationClasses/point2d.cpp \
    SimulationClasses/testdata.cpp \
    SimulationClasses/UdpClassLib/udpclient.cpp \
    SimulationClasses/UdpClassLib/udpserver.cpp \
    SimulationClasses/logdata.cpp

HEADERS += \
        widget.h \
    SimulationClasses/mission.h \
    SimulationClasses/point2d.h \
    SimulationClasses/testdata.h \
    SimulationClasses/allinclude.h \
    SimulationClasses/UdpClassLib/udpclient.h \
    SimulationClasses/UdpClassLib/udpserver.h \
    SimulationClasses/logdata.h

FORMS += \
        widget.ui
