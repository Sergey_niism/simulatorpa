#ifndef POINT2D_H
#define POINT2D_H



class Point2D
{
public:
    Point2D(double x = 0,double y = 0);

    double X;
    double Y;
};


class Point2D_Start:public Point2D
{
public:
    Point2D_Start(double x = 0,double y = 0,bool isDifferent = 0):Point2D(x,y){different = isDifferent;}

    bool different;

};

class Point2D_Finish:public Point2D
{
public:
    Point2D_Finish(double x = 0,double y = 0,bool isDifferent = 0):Point2D(x,y){different = isDifferent;}

    bool different;

};


#endif // POINT2D_H
