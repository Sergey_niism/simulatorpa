#ifndef LOGDATA_H
#define LOGDATA_H
#include <vector>
#include <fstream>




class LogData
{
public:
    LogData(int column);

    std::vector<std::vector<double>> data;
    std::vector<std::string> dataTitle;


    int column;
    int size;

    void setColumn(int column);
    int getSize();
    int getColumn();
    int addLine(std::vector<double> line);
    int toFileTXT(char* fileName);




};

#endif // LOGDATA_H
