#ifndef MISSION_H
#define MISSION_H
#include <SimulationClasses/point2d.h>
#include <vector>
#include <string>

class Mission
{
public:
    Mission();

    std::string Name;
    Point2D_Start pointStart;
    std::vector <Point2D> trajectory;
    Point2D_Finish pointFinish;

};

#endif // MISSION_H
