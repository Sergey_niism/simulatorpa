#ifndef UDPSERVER_H
#define UDPSERVER_H

#include <QObject>
#include <QWidget>
#include <QUdpSocket>
#include <QByteArray>

class UdpServer:public QWidget
{
    Q_OBJECT
public:
    UdpServer(int portNum,QWidget *parent = 0);

private:
    QUdpSocket* udpSocket;
    int socketPort;
    QHostAddress address = QHostAddress::LocalHost;

public slots:

    void SetSocketPort(int numberSocketPort);
    void SetHost(QHostAddress host);



    void sendDatagram();
    void sendDatagram(QByteArray data);
    void sendDatagram(QByteArray data, int portNumber);
    void sendDatagram(QString data);
    void sendDatagram(char* data);

    void sendPointInDatagram(double x, double y);
    void sendPointInDatagram(double x, double y,int portNumber);



};

#endif // UDPSERVER_H
