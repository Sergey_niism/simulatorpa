#ifndef UDPCLIENT_H
#define UDPCLIENT_H

#include <QObject>
#include <QWidget>
#include <QUdpSocket>
#include <QByteArray>
#include <QDebug>

class UdpClient:public QWidget
{
    Q_OBJECT

    Q_PROPERTY(int socketPort READ GetSocketPort WRITE SetSocketPort)

public:
    explicit UdpClient(int portNum, QWidget *parent = 0);
    QString qString;
    QByteArray GetData();
    int GetDataSize();
    int GetPointFromDatagram(double &x, double &y);

private:
    QUdpSocket* udpSocket;
    int socketPort;
    int dataSize;
    QByteArray datagramData;

    QHostAddress address = QHostAddress::LocalHost;


public slots:

    void SetHost(QHostAddress host);

    void takeDatagram();

    int GetSocketPort();
    void SetSocketPort(int NumberSocketPort );

signals:
    void S_takeDatagram();


};

#endif // UDPCLIENT_H
