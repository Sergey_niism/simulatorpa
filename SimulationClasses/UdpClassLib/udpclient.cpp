#include "udpclient.h"

UdpClient::UdpClient(int portNum, QWidget *parent):QWidget(parent)
{
    udpSocket = new QUdpSocket(this);
    socketPort = portNum;
    udpSocket->bind(address,socketPort);
    connect(udpSocket,SIGNAL(readyRead()),SLOT(takeDatagram()));

    datagramData.clear();

}

int UdpClient::GetDataSize()
{
    return dataSize;
}

QByteArray UdpClient::GetData()
{
    return datagramData;

}

void UdpClient::takeDatagram()
{
    QByteArray byteArray;
    while (udpSocket->hasPendingDatagrams())
    {
        dataSize = udpSocket->pendingDatagramSize();
        byteArray.resize(udpSocket->pendingDatagramSize());
        udpSocket->readDatagram(byteArray.data(),byteArray.size());

        datagramData = byteArray;

        qDebug()<<byteArray;
        QDataStream dataStream(&byteArray,QIODevice::ReadOnly);

        QString str;
        char* buf = new char[dataSize];
        dataStream >> buf;

        str = buf;



        qDebug()<<buf<<str;
        qString = str;



        emit S_takeDatagram();

    }

}

int UdpClient::GetPointFromDatagram(double &x, double &y)
{
    if (datagramData.isEmpty())
    {
        return 0;
    }
    QByteArray byteArray = datagramData;
    QDataStream in (&byteArray,QIODevice::ReadOnly);
    in >>x>>y;
    return 1;
}

void UdpClient::SetHost(QHostAddress host)
{
    address = host;
    udpSocket->close();
    udpSocket->bind(address,socketPort);

}

int UdpClient::GetSocketPort()
{
    return socketPort;
}

void UdpClient::SetSocketPort(int NumberSocketPort)
{
    socketPort = NumberSocketPort;
    udpSocket->close();
    udpSocket->bind(address,socketPort);

}
