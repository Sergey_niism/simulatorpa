#include "udpserver.h"

UdpServer::UdpServer(int portNum, QWidget *parent):QWidget(parent)
{
    udpSocket = new QUdpSocket(this);
    socketPort = portNum;

}

void UdpServer::SetSocketPort(int numberSocketPort)
{
    socketPort = numberSocketPort;
}

void UdpServer::SetHost(QHostAddress host)
{
    address = host;
}



void UdpServer::sendDatagram()
{
//    QByteArray byteArray;
//    QDataStream out(&byteArray,QIODevice::WriteOnly);
//    out<<" Пробная посылка";
    sendDatagram("Пробная посылка");
}

void UdpServer::sendDatagram(QByteArray data)
{
    udpSocket->writeDatagram(data,address, socketPort);

}

void UdpServer::sendDatagram(QByteArray data, int portNumber)
{
    udpSocket->writeDatagram(data,address, portNumber);
}

void UdpServer::sendDatagram(QString data)
{
    sendDatagram(data.toLocal8Bit());
}

void UdpServer::sendDatagram(char *data)
{
    QByteArray byteArray = data;
    sendDatagram(byteArray);
}

void UdpServer::sendPointInDatagram(double x, double y)
{
//    QByteArray byteArray;
//    QDataStream stream(&byteArray,QIODevice::WriteOnly);
//    stream<< x<<y;
//    sendDatagram(byteArray);
    sendPointInDatagram(x,y,socketPort);
}

void UdpServer::sendPointInDatagram(double x, double y, int portNumber)
{
    QByteArray byteArray;
    QDataStream stream(&byteArray,QIODevice::WriteOnly);
    stream<< x<<y;
    sendDatagram(byteArray,portNumber);

}
