#include "testdata.h"

testData::testData()
{

}

Mission testData::getTestMission()
{
    Mission mis;
    mis.pointStart.different = 1;
    mis.pointFinish.different = 0;

    mis.trajectory.resize(5);

    mis.trajectory.at(0).X = 50;
    mis.trajectory.at(0).Y = 50;
    mis.trajectory.at(1).X = 100;
    mis.trajectory.at(1).Y = 50;
    mis.trajectory.at(2).X = 100;
    mis.trajectory.at(2).Y = -50;
    mis.trajectory.at(3).X = 50;
    mis.trajectory.at(3).Y = -50;
    mis.trajectory.at(4).X = 0;
    mis.trajectory.at(4).Y = 0;
    return mis;
}
