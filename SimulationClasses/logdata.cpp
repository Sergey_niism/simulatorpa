#include "logdata.h"
#include <cmath>
#include <string>

LogData::LogData(int column)
{
    setColumn(column);

}

void LogData::setColumn(int column)
{
    this->column =column;
    for (int i = 0; i<data.size();i++)
        data.at(i).resize(this->column);

    dataTitle.resize(this->column);

    int j;
    while ( j <dataTitle.size())
    {
        if (j == 0) dataTitle.at(0) = "time";
        if (j == 1) dataTitle.at(1) = "X";
        if (j == 2) dataTitle.at(2) = "Y";
        if (j>2) dataTitle.at(2) = "ather";

        j++;
    }

}

int LogData::getSize()
{
    return size;

}

int LogData::getColumn()
{
    return column;
}

int LogData::addLine(std::vector<double> line)
{
     int flag =0;
     std::vector<double> buf;
     buf.clear();
     buf.resize(column);
     int i =0;
     for (i;i<std::min(line.size(),buf.size());i++)
     {
         buf.at(i) = line.at(i);
     }

     if (i<buf.size())
     {
         for (i;i<buf.size();i++)
         {
             buf.at(i) = 0;
         }
         flag = 1;
     }

     if (buf.size()<line.size())
         flag = 2;

     data.push_back(buf);
     size = data.size();

}

int LogData::toFileTXT(char *fileName)
{
    int f_parser =0;
    std::ofstream out;
    out.open(fileName);
    if (out.is_open() == 0)
    {
        return -1;
    }


// название колоннок
    for (int j =0; j< column;j++)
    {
        out<<dataTitle.at(j)<<"\t";
    }
    out<<std::endl;

//   заполнение данными
    for (int i =0; i< data.size();i++)
    {
        int j =0;
        for (j; j< column;j++)
        {
            if(j>(data.at(i).size()-1))
            {
                    f_parser = 1;
                    out<<"0"<<"\t";
            }
            out<<data.at(i).at(j)<<"\t";
        }
        if(j != data.at(i).size())
            f_parser = 2;

        out<<std::endl;
    }

    out.close();
    return f_parser;

}
