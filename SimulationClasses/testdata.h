#ifndef TESTDATA_H
#define TESTDATA_H

#include <SimulationClasses/allinclude.h>


class testData
{
public:
    testData();

    static Mission getTestMission ();
};

#endif // TESTDATA_H
